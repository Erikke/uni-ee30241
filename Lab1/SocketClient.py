# Import socket module
import socket			
import math
from PyQt5.QtWidgets import *
import sys
import time

class GraphicInterface(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.layout = QGridLayout()
        self.setLayout(self.layout)
        #First row
        self.layout.addWidget(QLabel("Welcome to"),0,0)
        self.layout.addWidget(QLabel("Erik's minimalist"),0,1)
        self.layout.addWidget(QLabel('Calculator'),0,2)
        #Second row
        self.inputOne =QLineEdit()
        self.inputOne.text()
        self.layout.addWidget(self.inputOne,1,0)
        self.operand = QComboBox(self)
        self.operand.addItem("+")
        self.operand.addItem("-")
        self.operand.addItem("*")
        self.operand.addItem("/")
        self.operand.addItem("^")
        self.operand.addItem("sqrt")
        self.layout.addWidget(self.operand,1,1)
        self.inputTwo =QLineEdit()
        self.inputTwo.text()
        self.layout.addWidget(self.inputTwo,1,2)
        self.submitButton = QPushButton("Execute")
        self.submitButton.clicked.connect(self.executeOperation)
        self.layout.addWidget(self.submitButton,1,3)
        #Third row
        self.layout.addWidget(QLabel("Output:"),2,0)
        self.outputWidget = QLabel("None")
        self.layout.addWidget(self.outputWidget,2,1)
        

        #Do the socket connection
        self.socketConn = SocketClient(12345)

    def executeOperation(self):
        print("Execution")
        print("Val1: " + str(self.inputOne.text()))
        self.socketConn.sendVal1(self.inputOne.text())
        time.sleep(0.1)
        print("Operand: " + str(self.operand.currentText()))
        self.socketConn.sendOperand(self.operand.currentText())
        time.sleep(0.1)
        print("Val2: " + str(self.inputTwo.text()))
        self.socketConn.sendVal2(self.inputTwo.text())
        time.sleep(0.1)
        self.displayOutput()
        
    def displayOutput(self):
        self.outputWidget.deleteLater()
        self.outputWidget = QLabel(self.socketConn.fetchServerData())
        self.layout.addWidget(self.outputWidget,2,1)
        
        


class SocketClient():
    def __init__(self, port):
        # Create a socket object
        self.currSocket = socket.socket()				

        # connect to the server on local computer
        self.currSocket.connect(('127.0.0.1', port))

    def sendVal1(self, val1):
        self.currSocket.send(val1.encode())

    def sendOperand(self, operand):
        self.currSocket.send(operand.encode())

    def sendVal2(self, val2):
        self.currSocket.send(val2.encode())

    def fetchServerData(self):
        return self.currSocket.recv(1024).decode()




app = QApplication(sys.argv)
screen = GraphicInterface()
screen.show()



sys.exit(app.exec_())



	


print("Please give first value:")
val1 = input()


print("Please give an operand:")
operand = input()


print("Please give second value:")
val2 = input()


print("Operation: ",val1,operand,val2)  
    
# receive data from the server and decoding to get the string.
print ()
# close the connection
s.close()	
	
