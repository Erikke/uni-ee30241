// Server Example c++

#include <iostream>
#include <WS2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")

#define PORT 10001
using namespace std;


int main()
{
    WSADATA SData;
    SOCKET listener_socket = NULL;
    SOCKET client = NULL;
    
    int WS_stat = WSAStartup(MAKEWORD(2, 2), &SData);

    sockaddr_in listener_address; // 
    sockaddr_in client_address;

    listener_address.sin_family = AF_INET;
    listener_address.sin_port = htons(PORT);
    listener_address.sin_addr.S_un.S_addr = INADDR_ANY;

    listener_socket = socket(AF_INET, SOCK_STREAM, 0);

    bind(listener_socket, (sockaddr*)&listener_address, sizeof(listener_address));
   
    listen(listener_socket, 5);
   
    cout << "Await client" << endl;

    client = accept(listener_socket, (sockaddr*)&client_address, &client_address_size);
    
    cout << "Client " <<client << " connected"<< endl;      
    send(client, "Welcome\n", 9, MSG_DONTROUTE);
 
    closesocket(listener_socket);
    closesocket(client);
    WSACleanup();
    
}