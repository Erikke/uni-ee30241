# first of all import the socket library
import socket			
import math
import os
import time

class LogWriter():
    def __init__(self, fileName):
        self.fileLocation = os.getcwd() + "/" + str(fileName)


    def addInit(self, port):
        with open(self.fileLocation, 'a') as write_file:
            write_file.write("Server started at port: " +str(port))
            write_file.write("\n")
    
    def addDate(self):
        localtime = time.asctime( time.localtime(time.time()) )
        #print "Local current time :", localtime
        with open(self.fileLocation, 'a') as write_file:
            write_file.write(">" + str(localtime))
            write_file.write("\n")

    def addOperand(self, operand):
        with open(self.fileLocation, 'a') as write_file:
            write_file.write(">>Operation request recieved")
            write_file.write("\n")
            write_file.write(">>>" +str(operand))
            write_file.write("\n")

    def addOutput(self, result):
        with open(self.fileLocation, 'a') as write_file:
            write_file.write(">>Operation finished")
            write_file.write("\n")
            write_file.write(">>>" +str(result))
            write_file.write("\n")
    
    def addClientConnection(self, clientIP):
        with open(self.fileLocation, 'a') as write_file:
            write_file.write("Connection successfully established with client at IP: ")
            write_file.write(str(clientIP))
            write_file.write("\n")






#start writing log
Log = LogWriter("SocketServerLog.txt")

# next create a socket object
s = socket.socket()	
print ("Socket successfully created")



# reserve a port on your computer in our
# case it is 12345 but it can be anything
port = 12345

# Next bind to the port
# we have not typed any ip in the ip field
# instead we have inputted an empty string
# this makes the server listen to requests
# coming from other computers on the network
s.bind(('', port))		
print ("socket binded to %s" %(port))

#Write Log for successful port initialization
Log.addDate()
Log.addInit(port)

# put the socket into listening mode
s.listen(5)	
print ("socket is listening")		

# a forever loop until we interrupt it or
# an error occurs
while True:

    # Establish connection with client.
    c, addr = s.accept()	
    print ('Got connection from', addr )

    #Write established connection to log
    Log.addDate()
    Log.addClientConnection(addr)

    while True:

        r_val1 = c.recv(1024).decode()
        print("Recieved val1 is:" + str(r_val1))

        r_operand = c.recv(1024).decode()
        print("Recieved operand is:" + str(r_operand))

        r_val2 = c.recv(1024).decode()
        print("Recieved val2 is:" + str(r_val2))

        message = r_val1 + r_operand + r_val2
        #Write operation input to log
        Log.addDate()
        Log.addOperand(message)

        if r_operand == "+":
            output = float(r_val1) + float(r_val2)
        elif r_operand =="-":
            output = float(r_val1) - float(r_val2)
        elif r_operand =="*":
            output = float(r_val1) * float(r_val2)
        elif r_operand =="/":
            output = float(r_val1) / float(r_val2)
        elif r_operand =="^":
            output = float(r_val1) ** float(r_val2)
        elif r_operand =="sqrt":
            output = math.sqrt(float(r_val1))
        else:
            output = "Error"

        print("message: ", message)
        print("output: ", output)

        # send a thank you message to the client. encoding to send byte type.
        c.send((str(output)).encode())

        #Write operation input to log
        Log.addDate()
        Log.addOutput(output)

    # Close the connection with the client
    c.close()

    # Breaking once connection closed
    #break
