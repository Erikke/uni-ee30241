//Client Example Cpp

#include <iostream>
#include <WS2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")

#define S_PORT 10001
using namespace std;

int main()
{

    WSADATA SData;
    SOCKET ToServer = NULL;
    
    char r_buffer[1024]{ 0 };
    sockaddr_in Connect_addr;
    Connect_addr.sin_family = AF_INET;
    Connect_addr.sin_port = htons(S_PORT);
    inet_pton(AF_INET, "127.0.0.1", &Connect_addr.sin_addr);

    WSAStartup(MAKEWORD(2, 2), &SData);

    ToServer = socket(AF_INET, SOCK_STREAM, 0);

    connect(ToServer, (sockaddr*)&Connect_addr, sizeof(Connect_addr));
    cout << "connected... " << endl;
    

    recv(ToServer, r_buffer, 9, NULL);
    cout << "Received : " << r_buffer << endl;

    cout << "Close connection" << endl;

    closesocket(ToServer);
    WSACleanup();
       
}